package bitlicon.neopet.Ecommerce.Product.View

import bitlicon.neopet.Entities.Category
import bitlicon.neopet.Entities.Product

interface ProductView {

    fun listProducts(products: MutableList<Product>)
    fun listCategories(categories: List<Category>)
}
package bitlicon.neopet.Ecommerce.Product.Repository

import bitlicon.neopet.Entities.Category
import java.util.ArrayList

import bitlicon.neopet.Entities.Product
import bitlicon.neopet.Ecommerce.Product.Presenter.ProductPresenter
import bitlicon.neopet.R

class ProductRepositoryImpl(private val productPresenter: ProductPresenter) : ProductRepository {

    override fun listProducts() {
        val products = ArrayList<Product>()
        products.add(Product(1, "Pedigree", "Perros", 1, 100.toDouble(), 100.toDouble(), "https://www.pedigree.com/images/default-source/Products/Dry/pedigree_adult-roasted-chicken_dry.tmb-400max.png?sfvrsn=a25a6e49_8https://www.pedigree.com/images/default-source/Products/Dry/pedigree_adult-roasted-chicken_dry.tmb-400max.png?sfvrsn=a25a6e49_8", "97% de selección única de trocitos y paté de pato.", "0.40kg"))
        products.add(Product(2, "Mimaskot", "Perros", 1, 110.toDouble(), 110.toDouble(), "https://alimentoanimal.files.wordpress.com/2010/12/mimaskot.jpg", "97% de selección única de trocitos y paté de pato.", "0.40kg"))
        products.add(Product(3, "Ricocan Clásico", "Perros", 1, 105.toDouble(), 105.toDouble(), "https://www.vetplace.pe/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/r/i/ricocan-adulto-clasico-todas-razas-22kg-alimento-balanceado-perros-2.jpg", "97% de selección única de trocitos y paté de pato.", "0.40kg"))
        products.add(Product(4, "Dog Chow", "Perros", 1, 100.toDouble(), 100.toDouble(), "https://www.purinalatam.com/img/dog_chow/mexico/always-forever/pack-sano.png", "97% de selección única de trocitos y paté de pato.", "0.40kg"))
        products.add(Product(5, "Master Dog", "Perros", 1, 115.toDouble(), 115.toDouble(), "http://newpaint.cl/wp-content/uploads/2017/03/MASTER-DOG-POLLO-18K.jpg", "97% de selección única de trocitos y paté de pato.", "0.40kg"))
        products.add(Product(6, "Ricocat", "Gatos", 1, 100.toDouble(), 100.toDouble(), "http://plazavea.vteximg.com.br/arquivos/ids/177556-220-220/20019997.jpg", "97% de selección única de trocitos y paté de pato.", "0.40kg"))
        products.add(Product(7, "Whiskas", "Gatos", 1, 110.toDouble(), 110.toDouble(), "https://www.whiskas.es/assets/img/home/pro/kitten.png", "97% de selección única de trocitos y paté de pato.", "0.40kg"))
        products.add(Product(8, "Cat Chow", "Perros", 1, 105.toDouble(), 105.toDouble(), "https://www.purinalatam.com/img/catchow/maquetacion/familia-de-productos/pack_adultos_carne.png", "97% de selección única de trocitos y paté de pato.", "0.40kg"))
        products.add(Product(9, "Friskies", "Gatos", 1, 60.toDouble(), 60.toDouble(), "https://m.media-amazon.com/images/S/aplus-media/vc/5d488d83-0685-4c7c-b0d8-9447ee79f43f._SL300__.png", "97% de selección única de trocitos y paté de pato.", "0.40kg"))
        products.add(Product(10, "Felix", "Gatos", 1, 75.toDouble(), 75.toDouble(), "https://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201601/29/00149510401810____2__210x210.jpg", "97% de selección única de trocitos y paté de pato.", "0.40kg"))

        productPresenter.sendProducts(products)
    }

    override fun listCategories() {
        val categories = ArrayList<Category>()
        categories.add(Category(1, R.drawable.ic_dog))
        categories.add(Category(2, R.drawable.ic_cat))
        productPresenter.sendCategories(categories)
    }
}
package bitlicon.neopet.Ecommerce.Product.Interactor

interface ProductInteractor {

    fun listProducts()
    fun listCategories()
}
package bitlicon.neopet.Ecommerce.Product.Presenter

import bitlicon.neopet.Entities.Category
import bitlicon.neopet.Entities.Product

interface ProductPresenter {

    fun listProducts()
    fun listCategories()
    fun sendProducts(products: MutableList<Product>)
    fun sendCategories(categories: List<Category>)
}
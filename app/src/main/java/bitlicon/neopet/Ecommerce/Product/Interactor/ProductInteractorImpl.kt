package bitlicon.neopet.Ecommerce.Product.Interactor

import bitlicon.neopet.Ecommerce.Product.Presenter.ProductPresenter
import bitlicon.neopet.Ecommerce.Product.Repository.ProductRepository
import bitlicon.neopet.Ecommerce.Product.Repository.ProductRepositoryImpl

class ProductInteractorImpl(productPresenter: ProductPresenter) : ProductInteractor {

    private val productRepository: ProductRepository

    init {
        productRepository = ProductRepositoryImpl(productPresenter)
    }

    override fun listProducts() {
        productRepository.listProducts()
    }

    override fun listCategories() {
        productRepository.listCategories()
    }
}
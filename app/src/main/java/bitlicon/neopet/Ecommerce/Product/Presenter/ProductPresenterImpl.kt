package bitlicon.neopet.Ecommerce.Product.Presenter

import bitlicon.neopet.Entities.Category
import bitlicon.neopet.Entities.Product
import bitlicon.neopet.Ecommerce.Product.Interactor.ProductInteractor
import bitlicon.neopet.Ecommerce.Product.Interactor.ProductInteractorImpl
import bitlicon.neopet.Ecommerce.Product.View.ProductView

class ProductPresenterImpl(private val productView: ProductView) : ProductPresenter {

    private val productInteractor: ProductInteractor

    init {
        productInteractor = ProductInteractorImpl(this)
    }

    override fun listProducts() {
        productInteractor.listProducts()
    }

    override fun listCategories() {
        productInteractor.listCategories()
    }

    override fun sendProducts(products: MutableList<Product>) {
        productView.listProducts(products)
    }

    override fun sendCategories(categories: List<Category>) {
        productView.listCategories(categories)
    }
}
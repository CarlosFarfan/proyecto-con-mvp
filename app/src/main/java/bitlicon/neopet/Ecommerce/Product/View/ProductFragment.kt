package bitlicon.neopet.Ecommerce.Product.View

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import bitlicon.neopet.Entities.Category

import com.mikepenz.actionitembadge.library.ActionItemBadge

import bitlicon.neopet.Entities.Product
import bitlicon.neopet.MainActivity
import bitlicon.neopet.Utils.Adapter.ProductAdapter
import bitlicon.neopet.Ecommerce.Product.Presenter.ProductPresenter
import bitlicon.neopet.Ecommerce.Product.Presenter.ProductPresenterImpl
import bitlicon.neopet.R
import bitlicon.neopet.Ecommerce.ShoppingCart.View.SCartActivity
import bitlicon.neopet.Utils.Adapter.CategoryAdapter

class ProductFragment : Fragment(), ProductView {

    private var recyclerViewProducts: RecyclerView? = null
    private var recyclerCategories: RecyclerView? = null

    private var productPresenter: ProductPresenter? = null
    private var categoryAdapter: CategoryAdapter? = null
    private var productAdapter: ProductAdapter? = null
    private var item: MenuItem? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater?.inflate(R.layout.fragment_product, container, false)
        setHasOptionsMenu(true)

        (activity as MainActivity).supportActionBar!!.setTitle(R.string.text_title_products)

        productPresenter = ProductPresenterImpl(this)

        recyclerViewProducts = view?.findViewById(R.id.recycler_products)
        recyclerCategories = view?.findViewById(R.id.recycler_categories)

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.main, menu)

        item = menu?.findItem(R.id.action_shop)
        ActionItemBadge.update(activity, item, ContextCompat.getDrawable(activity, R.drawable.ic_shop), ActionItemBadge.BadgeStyles.DARK_GREY, 0)
        productPresenter?.listCategories()
        productPresenter?.listProducts()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_shop) {
            startActivity(Intent(activity, SCartActivity::class.java))
            activity.finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun listProducts(products: MutableList<Product>) {
        productAdapter = ProductAdapter(products, activity, item!!, 1)
        recyclerViewProducts?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerViewProducts?.adapter = productAdapter
    }

    override fun listCategories(categories: List<Category>) {
        categoryAdapter = CategoryAdapter(activity, recyclerCategories!!, categories, -1)
        recyclerCategories?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        recyclerCategories?.adapter = categoryAdapter

        categoryAdapter?.setOnItemClickListener(object : CategoryAdapter.OnItemClickListener {

            override fun onClick(option: Int) {
                Toast.makeText(activity, option.toString(), Toast.LENGTH_SHORT).show()
            }
        })
    }
}
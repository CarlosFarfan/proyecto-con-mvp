package bitlicon.neopet.Ecommerce.Product.Repository

interface ProductRepository {

    fun listProducts()
    fun listCategories()
}
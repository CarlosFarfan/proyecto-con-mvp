package bitlicon.neopet.Ecommerce.ShoppingCart.Presenter

import bitlicon.neopet.Entities.Product

interface SCartPresenter {

    fun listProductsCart()
    fun sendProductsCart(products: MutableList<Product>, quantity: List<Int>)
}
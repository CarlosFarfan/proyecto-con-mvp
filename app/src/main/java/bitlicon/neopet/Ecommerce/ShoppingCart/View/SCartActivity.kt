package bitlicon.neopet.Ecommerce.ShoppingCart.View

import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView

import com.mikepenz.actionitembadge.library.ActionItemBadge
import java.util.Timer
import java.util.TimerTask

import bitlicon.neopet.Entities.Product
import bitlicon.neopet.MainActivity
import bitlicon.neopet.User.PaymentDetail.View.PaymentDetailActivity
import bitlicon.neopet.R
import bitlicon.neopet.Ecommerce.ShoppingCart.Adapter.SCartAdapter
import bitlicon.neopet.Ecommerce.ShoppingCart.Presenter.SCartPresenter
import bitlicon.neopet.Ecommerce.ShoppingCart.Presenter.SCartPresenterImpl
import bitlicon.neopet.Utils.Functions
import bitlicon.neopet.Utils.Preferences

class SCartActivity : AppCompatActivity(), SCartView {

    private var textSubTotal: TextView? = null
    private var textIGV: TextView? = null
    private var textTotal: TextView? = null
    private var recyclerView: RecyclerView? = null

    private var sCartPresenter: SCartPresenter? = null
    private var sCartAdapter: SCartAdapter? = null
    private var preferences: Preferences? = null
    private var item: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scart)

        preferences = Preferences(this@SCartActivity)

        sCartPresenter = SCartPresenterImpl(this)
        val toolbar = findViewById<Toolbar>(R.id.t_cart)
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.text_title_cart)
        toolbar.navigationIcon = ContextCompat.getDrawable(this@SCartActivity, R.drawable.ic_home_up)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        textSubTotal = findViewById(R.id.quantity_sub_total_cart)
        textIGV = findViewById(R.id.quantity_igv_cart)
        textTotal = findViewById(R.id.quantity_total_cart)
        recyclerView = findViewById(R.id.recycler_products_cart)
        val btnBuy = findViewById<Button>(R.id.btn_buy_products_cart)

        btnBuy.setOnClickListener {
            val intent = Intent(this@SCartActivity, PaymentDetailActivity::class.java)
            intent.putExtra("TotalPay", textTotal!!.text)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)

        item = menu.findItem(R.id.action_shop)
        ActionItemBadge.update(this@SCartActivity, item, ContextCompat.getDrawable(this@SCartActivity, R.drawable.ic_shop), ActionItemBadge.BadgeStyles.DARK_GREY, 0)
        sCartPresenter?.listProductsCart()
        return super.onCreateOptionsMenu(menu)
    }

    override fun listProductsCart(products: MutableList<Product>, quantity: List<Int>) {
        sCartAdapter = SCartAdapter(products, quantity, this@SCartActivity, item!!)
        recyclerView?.layoutManager = LinearLayoutManager(this@SCartActivity, LinearLayoutManager.VERTICAL, false)
        recyclerView?.adapter = sCartAdapter
        followPrices()
    }

    private fun followPrices() {
        val timer = Timer()
        val timerTask = object : TimerTask() {
            override fun run() {
                val products = sCartAdapter!!.getProducts()
                val subTotal: Double?
                val igv: Double?
                var total: Double? = 0.00
                val finalTotal: Double?
                for (i in products.indices) {
                    total = total?.plus(products[i].priceByQuantity!!)
                }

                igv = total!! * 0.18
                subTotal = total - igv
                finalTotal = total
                runOnUiThread {
                    textSubTotal?.text = "S/." + Functions.formatPrice(subTotal)
                    textIGV?.text = "S/." + Functions.formatPrice(igv)
                    textTotal?.text = "S/." + Functions.formatPrice(finalTotal)
                }
            }
        }
        timer.scheduleAtFixedRate(timerTask, 0, 1000)
    }

    override fun onBackPressed() {
        preferences?.showProducts(true)
        preferences?.showMyPurchases(false)
        startActivity(Intent(this@SCartActivity, MainActivity::class.java))
        finish()
    }
}
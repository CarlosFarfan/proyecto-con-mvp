package bitlicon.neopet.Ecommerce.ShoppingCart.Adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView

import com.bumptech.glide.Glide

import bitlicon.neopet.Entities.Product
import bitlicon.neopet.Ecommerce.ProductDetail.View.ProductDetailActivity
import bitlicon.neopet.R
import bitlicon.neopet.Utils.Functions

class SCartAdapter(private val products: MutableList<Product>, private val quantity: List<Int>, private val activity: Activity, private val item: MenuItem) : RecyclerView.Adapter<SCartAdapter.SCartViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SCartAdapter.SCartViewHolder {
        return SCartViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_cart, parent, false))
    }

    override fun onBindViewHolder(holder: SCartAdapter.SCartViewHolder, position: Int) {
        val product = products[position]

        Glide.with(activity).load(product.image).into(holder.imageProduct)
        holder.product.text = product.product
        holder.typeAnimal.text = product.typeAnimal

        val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, quantity)
        adapter.setDropDownViewResource(R.layout.item_spinner)
        holder.spSelectQuantity.adapter = adapter
        holder.spSelectQuantity.setSelection(if (product.quantity == 0) 0 else product.quantity - 1)

        holder.spSelectQuantity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                product.quantity = position + 1
                val price = product.price!! * product.quantity
                product.priceByQuantity = price
                holder.priceProduct.text = "S/." + Functions.formatPrice(price)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        holder.btnRemoveProduct.setOnClickListener {
            products.removeAt(holder.adapterPosition)
            notifyDataSetChanged()
        }
    }

    inner class SCartViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        internal val product: TextView = view.findViewById(R.id.product_cart)
        internal val typeAnimal: TextView = view.findViewById(R.id.type_animal_cart)
        internal val priceProduct: TextView = view.findViewById(R.id.price_product_cart)
        internal val spSelectQuantity: Spinner = view.findViewById(R.id.spinner_select_quantity_cart)
        internal val btnRemoveProduct: Button = view.findViewById(R.id.btn_remove_product_cart)
        internal val imageProduct: ImageView = view.findViewById(R.id.image_product_cart)

        init {
            view.setOnClickListener {
                val intent = Intent(activity, ProductDetailActivity::class.java)
                intent.putExtra("EnterID", 3)
                activity.startActivity(intent)
                activity.finish()
            }
        }
    }

    override fun getItemCount(): Int {
        return products.size
    }

    fun getProducts(): List<Product> {
        return products
    }
}
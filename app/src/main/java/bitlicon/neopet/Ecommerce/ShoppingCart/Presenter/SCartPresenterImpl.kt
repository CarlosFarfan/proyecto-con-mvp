package bitlicon.neopet.Ecommerce.ShoppingCart.Presenter

import bitlicon.neopet.Entities.Product
import bitlicon.neopet.Ecommerce.ShoppingCart.Interactor.SCartInteractor
import bitlicon.neopet.Ecommerce.ShoppingCart.Interactor.SCartInteractorImpl
import bitlicon.neopet.Ecommerce.ShoppingCart.View.SCartView

class SCartPresenterImpl(private val SCartView: SCartView) : SCartPresenter {

    private val SCartInteractor: SCartInteractor

    init {
        SCartInteractor = SCartInteractorImpl(this)
    }

    override fun listProductsCart() {
        SCartInteractor.listProductsCart()
    }

    override fun sendProductsCart(products: MutableList<Product>, quantity: List<Int>) {
        SCartView.listProductsCart(products, quantity)
    }
}
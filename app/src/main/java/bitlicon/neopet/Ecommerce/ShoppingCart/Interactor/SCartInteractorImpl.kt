package bitlicon.neopet.Ecommerce.ShoppingCart.Interactor

import bitlicon.neopet.Ecommerce.ShoppingCart.Presenter.SCartPresenter
import bitlicon.neopet.Ecommerce.ShoppingCart.Repository.SCartRepository
import bitlicon.neopet.Ecommerce.ShoppingCart.Repository.SCartRepositoryImpl

class SCartInteractorImpl(SCartPresenter: SCartPresenter) : SCartInteractor {

    private val productRepository: SCartRepository

    init {
        productRepository = SCartRepositoryImpl(SCartPresenter)
    }

    override fun listProductsCart() {
        productRepository.listProductsCart()
    }
}
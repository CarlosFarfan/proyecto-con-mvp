package bitlicon.neopet.Ecommerce.ShoppingCart.View

import bitlicon.neopet.Entities.Product

interface SCartView {

    fun listProductsCart(products: MutableList<Product>, quantity: List<Int>)
}
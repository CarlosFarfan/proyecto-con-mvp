package bitlicon.neopet.Ecommerce.DetailPurchase.Interactor

import bitlicon.neopet.Ecommerce.DetailPurchase.Presenter.DPPresenter
import bitlicon.neopet.Ecommerce.DetailPurchase.Repository.DPRepository
import bitlicon.neopet.Ecommerce.DetailPurchase.Repository.DPRepositoryImpl

class DPInteractorImpl(dpPresenter: DPPresenter) : DPInteractor {

    private val dpRepository: DPRepository

    init {
        dpRepository = DPRepositoryImpl(dpPresenter)
    }

    override fun listProducts() {
        dpRepository.listProducts()
    }
}
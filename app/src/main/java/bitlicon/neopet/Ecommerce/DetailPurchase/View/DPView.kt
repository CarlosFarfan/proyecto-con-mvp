package bitlicon.neopet.Ecommerce.DetailPurchase.View

import bitlicon.neopet.Entities.Product

interface DPView {

    fun showTexts()
    fun listProducts(products: MutableList<Product>)
}
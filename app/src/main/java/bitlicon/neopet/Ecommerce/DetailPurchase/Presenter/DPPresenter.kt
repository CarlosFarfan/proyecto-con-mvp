package bitlicon.neopet.Ecommerce.DetailPurchase.Presenter

import bitlicon.neopet.Entities.Product

interface DPPresenter {

    fun showTexts()
    fun listProducts()
    fun sendProducts(products: MutableList<Product>)
}
package bitlicon.neopet.Ecommerce.DetailPurchase.View

import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView

import com.mikepenz.actionitembadge.library.ActionItemBadge

import bitlicon.neopet.Ecommerce.DetailPurchase.Presenter.DPPresenter
import bitlicon.neopet.Ecommerce.DetailPurchase.Presenter.DPPresenterImpl
import bitlicon.neopet.Entities.Product
import bitlicon.neopet.MainActivity
import bitlicon.neopet.R
import bitlicon.neopet.Ecommerce.ShoppingCart.View.SCartActivity
import bitlicon.neopet.Utils.Adapter.ProductAdapter
import bitlicon.neopet.Utils.Functions
import bitlicon.neopet.Utils.Preferences

class DetailPurchaseActivity : AppCompatActivity(), DPView {

    private var recyclerView: RecyclerView? = null
    private var tvDate: TextView? = null
    private var tvTotal: TextView? = null

    private var productAdapter: ProductAdapter? = null
    private var preferences: Preferences? = null
    private var dpPresenter: DPPresenter? = null
    private var item: MenuItem? = null
    private var Total: Double? = null
    private var Date: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_purchase)

        preferences = Preferences(this@DetailPurchaseActivity)

        Total = intent.getDoubleExtra("Total", 0.00)
        Date = intent.getStringExtra("Date")

        dpPresenter = DPPresenterImpl(this)

        val toolbar: Toolbar = findViewById(R.id.t_detail_purchase)
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.text_title_purchase)

        toolbar.navigationIcon = ContextCompat.getDrawable(this@DetailPurchaseActivity, R.drawable.ic_home_up)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        tvDate = findViewById(R.id.date_detail_purchase)
        tvTotal = findViewById(R.id.quantity_total_detail_purchase)
        recyclerView = findViewById(R.id.recycler_products_detail_purchase)
        val btnAddAll = findViewById<Button>(R.id.btn_add_products_detail_purchase)

        btnAddAll.setOnClickListener { productAdapter?.addAll() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)

        item = menu.findItem(R.id.action_shop)
        ActionItemBadge.update(this@DetailPurchaseActivity, item, ContextCompat.getDrawable(this@DetailPurchaseActivity, R.drawable.ic_shop), ActionItemBadge.BadgeStyles.DARK_GREY, 0)
        dpPresenter?.listProducts()
        dpPresenter?.showTexts()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_shop -> {
                startActivity(Intent(this@DetailPurchaseActivity, SCartActivity::class.java))
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        preferences?.showMyPurchases(true)
        preferences?.showProducts(false)
        startActivity(Intent(this@DetailPurchaseActivity, MainActivity::class.java))
        finish()
    }

    override fun showTexts() {
        tvDate?.text = Date
        tvTotal?.text = Functions.formatPrice(Total)
    }

    override fun listProducts(products: MutableList<Product>) {
        productAdapter = ProductAdapter(products, this@DetailPurchaseActivity, item!!, 2)
        recyclerView?.layoutManager = LinearLayoutManager(this@DetailPurchaseActivity, LinearLayoutManager.VERTICAL, false)
        recyclerView?.adapter = productAdapter
    }
}
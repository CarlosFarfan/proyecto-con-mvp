package bitlicon.neopet.Ecommerce.DetailPurchase.Presenter

import bitlicon.neopet.Ecommerce.DetailPurchase.Interactor.DPInteractor
import bitlicon.neopet.Ecommerce.DetailPurchase.Interactor.DPInteractorImpl
import bitlicon.neopet.Ecommerce.DetailPurchase.View.DPView
import bitlicon.neopet.Entities.Product

class DPPresenterImpl(private val dpView: DPView) : DPPresenter {

    private val dpInteractor: DPInteractor

    init {
        dpInteractor = DPInteractorImpl(this)
    }

    override fun showTexts() {
        dpView.showTexts()
    }

    override fun listProducts() {
        dpInteractor.listProducts()
    }

    override fun sendProducts(products: MutableList<Product>) {
        dpView.listProducts(products)
    }
}
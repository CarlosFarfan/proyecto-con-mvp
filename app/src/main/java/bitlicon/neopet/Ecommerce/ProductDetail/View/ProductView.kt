package bitlicon.neopet.Ecommerce.ProductDetail.View

import bitlicon.neopet.Entities.Product

interface ProductView {

    fun showProduct(product: Product)
}
package bitlicon.neopet.Ecommerce.ProductDetail.Repository

import bitlicon.neopet.Entities.Product
import bitlicon.neopet.Ecommerce.ProductDetail.Presenter.ProductPresenter

class ProductRepositoryImpl(private val productPresenter: ProductPresenter) : ProductRepository {

    override fun showProduct() {
        val product = Product(
                1,
                "Mimaskot",
                "Perro",
                1,
                125.50,
                125.50,
                "https://alimentoanimal.files.wordpress.com/2010/12/mimaskot.jpg",
                "97% de selección única de trocitos y paté de pato.",
                "10kg"
        )

        productPresenter.sendProduct(product)
    }
}
package bitlicon.neopet.Ecommerce.ProductDetail.View

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.widget.ImageView
import android.widget.TextView
import bitlicon.neopet.Ecommerce.DetailPurchase.View.DetailPurchaseActivity
import bitlicon.neopet.Ecommerce.ProductDetail.Presenter.ProductPresenter
import bitlicon.neopet.Ecommerce.ProductDetail.Presenter.ProductPresenterImpl
import bitlicon.neopet.Entities.Product
import bitlicon.neopet.MainActivity
import bitlicon.neopet.R
import bitlicon.neopet.Ecommerce.ShoppingCart.View.SCartActivity
import bitlicon.neopet.Utils.Functions
import bitlicon.neopet.Utils.Preferences
import com.bumptech.glide.Glide

class ProductDetailActivity : AppCompatActivity(), ProductView {

    private var availabilityProduct: TextView? = null
    private var descriptionProduct: TextView? = null
    private var weightProduct: TextView? = null
    private var imageProduct: ImageView? = null
    private var priceProduct: TextView? = null
    private var nameProduct: TextView? = null

    private var productPresenter: ProductPresenter? = null
    private var preferences: Preferences? = null
    private var enterID: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        preferences = Preferences(this@ProductDetailActivity)
        enterID = intent.getIntExtra("EnterID", 0)
        productPresenter = ProductPresenterImpl(this)

        val toolbar: Toolbar = findViewById(R.id.t_product_detail)
        setSupportActionBar(toolbar)

        toolbar.navigationIcon = ContextCompat.getDrawable(this@ProductDetailActivity, R.drawable.ic_home_up)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        supportActionBar?.title = getString(R.string.text_title_product)

        imageProduct = findViewById(R.id.image_product_detail)
        nameProduct = findViewById(R.id.name_product_detail)
        availabilityProduct = findViewById(R.id.availability_product_detail)
        priceProduct = findViewById(R.id.price_product_detail)
        weightProduct = findViewById(R.id.weight_product_detail)
        descriptionProduct = findViewById(R.id.description_product_detail)

        productPresenter?.showProduct()
    }

    override fun showProduct(product: Product) {

        Glide.with(this@ProductDetailActivity).load(product.image).into(imageProduct)

        nameProduct?.text = product.product

        if (product.availability == 1) {
            availabilityProduct?.text = "En stock"
        } else {
            availabilityProduct?.text = "No hay"
        }

        priceProduct?.text = "S/.${Functions.formatPrice(product.price)}"
        weightProduct?.text = product.weight
        descriptionProduct?.text = product.shortInformation
    }

    override fun onBackPressed() {

        when (enterID) {
            1 -> {//Principal
                preferences?.showProducts(true)
                preferences?.showMyPurchases(false)
                startActivity(Intent(this@ProductDetailActivity, MainActivity::class.java))
                finish()
            }
            2 -> {//Detail Purchase
                startActivity(Intent(this@ProductDetailActivity, DetailPurchaseActivity::class.java))
                finish()
            }
            else -> {//Shopping Cart
                startActivity(Intent(this@ProductDetailActivity, SCartActivity::class.java))
                finish()
            }
        }
    }
}
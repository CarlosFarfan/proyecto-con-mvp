package bitlicon.neopet.Ecommerce.ProductDetail.Presenter

import bitlicon.neopet.Entities.Product

interface ProductPresenter {

    fun showProduct()
    fun sendProduct(product: Product)
}
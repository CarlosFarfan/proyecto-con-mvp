package bitlicon.neopet.Ecommerce.ProductDetail.Interactor

import bitlicon.neopet.Ecommerce.ProductDetail.Presenter.ProductPresenter
import bitlicon.neopet.Ecommerce.ProductDetail.Repository.ProductRepository
import bitlicon.neopet.Ecommerce.ProductDetail.Repository.ProductRepositoryImpl

class ProductInteractorImpl(productPresenter: ProductPresenter) : ProductInteractor {

    private val productRepository: ProductRepository

    init {
        productRepository = ProductRepositoryImpl(productPresenter)
    }

    override fun showProducts() {
        productRepository.showProduct()
    }
}
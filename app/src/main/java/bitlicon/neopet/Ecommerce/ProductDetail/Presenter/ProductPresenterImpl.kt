package bitlicon.neopet.Ecommerce.ProductDetail.Presenter

import bitlicon.neopet.Entities.Product
import bitlicon.neopet.Ecommerce.ProductDetail.Interactor.ProductInteractor
import bitlicon.neopet.Ecommerce.ProductDetail.Interactor.ProductInteractorImpl
import bitlicon.neopet.Ecommerce.ProductDetail.View.ProductView

class ProductPresenterImpl(private val productView: ProductView) : ProductPresenter {

    private val productInteractor: ProductInteractor

    init {
        productInteractor = ProductInteractorImpl(this)
    }

    override fun showProduct() {
        productInteractor.showProducts()
    }

    override fun sendProduct(product: Product) {
        productView.showProduct(product)
    }
}
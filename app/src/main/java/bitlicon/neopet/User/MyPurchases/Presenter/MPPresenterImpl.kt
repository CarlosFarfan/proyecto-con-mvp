package bitlicon.neopet.User.MyPurchases.Presenter

import bitlicon.neopet.Entities.Purchase
import bitlicon.neopet.User.MyPurchases.Interactor.MPInteractor
import bitlicon.neopet.User.MyPurchases.Interactor.MPInteractorImpl
import bitlicon.neopet.User.MyPurchases.View.MPView

class MPPresenterImpl(private val mpView: MPView) : MPPresenter {

    private val mpInteractor: MPInteractor

    init {
        mpInteractor = MPInteractorImpl(this)
    }

    override fun listPurchases() {
        mpInteractor.listPurchases()
    }

    override fun sendPurchases(purchases: List<Purchase>) {
        mpView.listPurchases(purchases)
    }
}
package bitlicon.neopet.User.MyPurchases.View

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import bitlicon.neopet.Entities.Purchase
import bitlicon.neopet.MainActivity
import bitlicon.neopet.User.MyPurchases.Adapter.MPAdapter
import bitlicon.neopet.User.MyPurchases.Presenter.MPPresenter
import bitlicon.neopet.User.MyPurchases.Presenter.MPPresenterImpl
import bitlicon.neopet.R

class MyPurchasesFragment : Fragment(), MPView {

    private var recyclerView: RecyclerView? = null

    private var mpPresenter: MPPresenter? = null
    private var mpAdapter: MPAdapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater?.inflate(R.layout.fragment_my_purchases, container, false)

        (activity as MainActivity).supportActionBar?.setTitle(R.string.text_title_my_purchases)
        mpPresenter = MPPresenterImpl(this)

        recyclerView = view?.findViewById(R.id.recycler_products_my_shoppings)

        mpPresenter?.listPurchases()
        return view
    }

    override fun listPurchases(purchases: List<Purchase>) {
        mpAdapter = MPAdapter(purchases, activity)
        recyclerView?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView?.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        recyclerView?.adapter = mpAdapter
    }
}
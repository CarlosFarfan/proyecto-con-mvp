package bitlicon.neopet.User.MyPurchases.Interactor

import bitlicon.neopet.User.MyPurchases.Presenter.MPPresenter
import bitlicon.neopet.User.MyPurchases.Repository.MPRepository
import bitlicon.neopet.User.MyPurchases.Repository.MPRepositoryImpl

class MPInteractorImpl(mpPresenter: MPPresenter) : MPInteractor {

    private val mpRepository: MPRepository

    init {
        mpRepository = MPRepositoryImpl(mpPresenter)
    }

    override fun listPurchases() {
        mpRepository.listPurchases()
    }
}

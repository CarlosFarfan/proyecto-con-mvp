package bitlicon.neopet.User.MyPurchases.Presenter

import bitlicon.neopet.Entities.Purchase

interface MPPresenter {

    fun listPurchases()
    fun sendPurchases(purchases: List<Purchase>)
}
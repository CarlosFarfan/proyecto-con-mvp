package bitlicon.neopet.User.MyPurchases.Repository

import java.util.ArrayList

import bitlicon.neopet.Entities.Purchase
import bitlicon.neopet.User.MyPurchases.Presenter.MPPresenter

class MPRepositoryImpl(private val mpPresenter: MPPresenter) : MPRepository {

    override fun listPurchases() {
        val purchases = ArrayList<Purchase>()
        purchases.add(Purchase(1, "03/03/2017", 1100.toDouble()))
        purchases.add(Purchase(2, "04/04/2017", 1101.toDouble()))
        purchases.add(Purchase(3, "05/05/2017", 1102.toDouble()))
        purchases.add(Purchase(4, "06/06/2017", 1103.toDouble()))
        purchases.add(Purchase(5, "07/07/2017", 1104.toDouble()))
        purchases.add(Purchase(6, "08/08/2017", 1105.toDouble()))
        purchases.add(Purchase(7, "09/09/2017", 1106.toDouble()))
        purchases.add(Purchase(8, "10/10/2017", 1107.toDouble()))
        purchases.add(Purchase(9, "11/11/2017", 1108.toDouble()))
        purchases.add(Purchase(10, "12/12/2017", 1109.toDouble()))

        mpPresenter.sendPurchases(purchases)
    }
}
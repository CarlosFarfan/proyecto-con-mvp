package bitlicon.neopet.User.MyPurchases.View

import bitlicon.neopet.Entities.Purchase

interface MPView {

    fun listPurchases(purchases: List<Purchase>)
}
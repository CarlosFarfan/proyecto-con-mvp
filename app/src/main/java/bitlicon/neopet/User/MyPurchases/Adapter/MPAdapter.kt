package bitlicon.neopet.User.MyPurchases.Adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import bitlicon.neopet.Ecommerce.DetailPurchase.View.DetailPurchaseActivity
import bitlicon.neopet.Entities.Purchase
import bitlicon.neopet.R
import bitlicon.neopet.Utils.Functions

class MPAdapter(private val purchases: List<Purchase>, private val activity: Activity) : RecyclerView.Adapter<MPAdapter.MPViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MPAdapter.MPViewHolder {
        return MPViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_my_purchase, parent, false))
    }

    override fun onBindViewHolder(holder: MPAdapter.MPViewHolder, position: Int) {
        val purchase = purchases[position]

        holder.date.text = purchase.date
        holder.total.text = "S/." + Functions.formatPrice(purchase.total)

        holder.btnSee.setOnClickListener {
            val intent = Intent(activity, DetailPurchaseActivity::class.java)
            intent.putExtra("Date", purchase.date)
            intent.putExtra("Total", purchase.total)
            activity.startActivity(intent)
            activity.finish()
        }
    }

    inner class MPViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal val date: TextView = view.findViewById(R.id.date_purchase)
        internal val total: TextView = view.findViewById(R.id.total_pay_purchase)
        internal val btnSee: Button = view.findViewById(R.id.btn_see_purchase)

    }

    override fun getItemCount(): Int {
        return purchases.size
    }
}
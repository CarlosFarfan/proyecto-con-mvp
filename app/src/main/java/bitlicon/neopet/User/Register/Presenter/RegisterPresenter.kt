package bitlicon.neopet.User.Register.Presenter

interface RegisterPresenter {

    fun signUp(email: String, password: String, repeatPassword: String, check: Boolean)
    fun goLogin()
    fun signUpSuccess()
    fun signUpError(error: String)
    fun hasFocus(hasFocus: Boolean, id: Int)
}
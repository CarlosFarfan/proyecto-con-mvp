package bitlicon.neopet.User.Register.Repository

import android.util.Patterns

import bitlicon.neopet.User.Register.Presenter.RegisterPresenter

class RegisterRepositoryImpl(private val registerPresenter: RegisterPresenter) : RegisterRepository {

    override fun sigUp(email: String, password: String, repeatPassword: String, check: Boolean) {

        if (email.isEmpty()) {
            registerPresenter.signUpError("Email vacío")
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            registerPresenter.signUpError("Email incorrecto")
        } else if (password.isEmpty()) {
            registerPresenter.signUpError("Escribir contraseña")
        } else if (password != repeatPassword) {
            registerPresenter.signUpError("Contraseñas incorrectas")
        } else if (!check) {
            registerPresenter.signUpError("Acepta las condiciones")
        } else {
            registerPresenter.signUpSuccess()
        }
    }
}
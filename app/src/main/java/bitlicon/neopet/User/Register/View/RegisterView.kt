package bitlicon.neopet.User.Register.View

interface RegisterView {

    fun goHome()
    fun goLogin()
    fun showProgressBar()
    fun hideProgressBar()
    fun signUpError(error: String)
    fun hasFocus(hasFocus: Boolean, id: Int)
    fun noHasFocus(hasFocus: Boolean, id: Int)
}
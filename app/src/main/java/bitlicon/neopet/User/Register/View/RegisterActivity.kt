package bitlicon.neopet.User.Register.View

import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageButton

import bitlicon.neopet.User.Login.View.LoginActivity
import bitlicon.neopet.MainActivity
import bitlicon.neopet.R
import bitlicon.neopet.User.Register.Presenter.RegisterPresenter
import bitlicon.neopet.User.Register.Presenter.RegisterPresenterImpl
import bitlicon.neopet.Utils.Functions

class RegisterActivity : AppCompatActivity(), RegisterView {

    private var flEmail: FrameLayout? = null
    private var flPassword: FrameLayout? = null
    private var flRepeatPassword: FrameLayout? = null
    private var etEmail: EditText? = null
    private var etPassword: EditText? = null
    private var etRepeatPassword: EditText? = null

    private var registerPresenter: RegisterPresenter? = null
    private var dialog: AlertDialog? = null

    private var checkCondition: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        registerPresenter = RegisterPresenterImpl(this)

        val ibHomeUp = findViewById<ImageButton>(R.id.ib_home_up_register)
        flEmail = findViewById(R.id.fl_email_register)
        flPassword = findViewById(R.id.fl_password_register)
        flRepeatPassword = findViewById(R.id.fl_repeat_password_register)
        etEmail = findViewById(R.id.et_email_register)
        etPassword = findViewById(R.id.et_password_register)
        etRepeatPassword = findViewById(R.id.et_repeat_password_register)
        val cbConditions = findViewById<CheckBox>(R.id.cb_accept_conditions)
        val btnSignUp = findViewById<Button>(R.id.btn_sign_up)

        etEmail?.setOnFocusChangeListener { _, hasFocus -> registerPresenter?.hasFocus(hasFocus, R.id.et_email_register) }
        etPassword?.setOnFocusChangeListener { _, hasFocus -> registerPresenter?.hasFocus(hasFocus, R.id.et_password_register) }
        etRepeatPassword?.setOnFocusChangeListener { _, hasFocus -> registerPresenter?.hasFocus(hasFocus, R.id.et_repeat_password_register) }

        cbConditions.setOnCheckedChangeListener { _, b -> checkCondition = b }

        ibHomeUp.setOnClickListener { registerPresenter?.goLogin() }
        btnSignUp.setOnClickListener { registerPresenter?.signUp(etEmail?.text.toString().trim(), etPassword?.text.toString().trim(), etRepeatPassword?.text.toString().trim(), checkCondition) }
    }

    override fun showProgressBar() {
        dialog = Functions.showProgressDialog("Registrando", "Espere un momento", this@RegisterActivity)
    }

    override fun hideProgressBar() {
        dialog?.dismiss()
    }

    override fun signUpError(error: String) {
        Functions.showAlertDialog(error, this@RegisterActivity)
    }

    override fun hasFocus(hasFocus: Boolean, id: Int) {
        when (id) {
            R.id.et_email_register -> flEmail?.background = ContextCompat.getDrawable(this@RegisterActivity, R.drawable.background_high_card)
            R.id.et_password_register -> flPassword?.background = ContextCompat.getDrawable(this@RegisterActivity, R.drawable.background_high_card)
            R.id.et_repeat_password_register -> flRepeatPassword?.background = ContextCompat.getDrawable(this@RegisterActivity, R.drawable.background_high_card)
        }
    }

    override fun noHasFocus(hasFocus: Boolean, id: Int) {
        when (id) {
            R.id.et_email_register -> flEmail?.background = ContextCompat.getDrawable(this@RegisterActivity, R.drawable.background_low_card)
            R.id.et_password_register -> flPassword?.background = ContextCompat.getDrawable(this@RegisterActivity, R.drawable.background_low_card)
            R.id.et_repeat_password_register -> flRepeatPassword?.background = ContextCompat.getDrawable(this@RegisterActivity, R.drawable.background_low_card)
        }
    }

    override fun goHome() {
        startActivity(Intent(this@RegisterActivity, MainActivity::class.java))
        finish()
    }

    override fun goLogin() {
        startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
        finish()
    }
}
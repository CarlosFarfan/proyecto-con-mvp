package bitlicon.neopet.User.Register.Interactor

import bitlicon.neopet.User.Register.Presenter.RegisterPresenter
import bitlicon.neopet.User.Register.Repository.RegisterRepository
import bitlicon.neopet.User.Register.Repository.RegisterRepositoryImpl

class RegisterInteractorImpl(private val registerPresenter: RegisterPresenter) : RegisterInteractor {

    private val registerRepository: RegisterRepository

    init {
        registerRepository = RegisterRepositoryImpl(registerPresenter)
    }

    override fun signUp(email: String, password: String, repeatPassword: String, check: Boolean) {
        registerRepository.sigUp(email, password, repeatPassword, check)
    }
}
package bitlicon.neopet.User.Register.Interactor

interface RegisterInteractor {

    fun signUp(email: String, password: String, repeatPassword: String, check: Boolean)
}
package bitlicon.neopet.User.Register.Presenter

import bitlicon.neopet.User.Register.Interactor.RegisterInteractor
import bitlicon.neopet.User.Register.Interactor.RegisterInteractorImpl
import bitlicon.neopet.User.Register.View.RegisterView

class RegisterPresenterImpl(private val registerView: RegisterView) : RegisterPresenter {

    private val registerInteractor: RegisterInteractor

    init {
        registerInteractor = RegisterInteractorImpl(this)
    }

    override fun signUp(email: String, password: String, repeatPassword: String, check: Boolean) {
        registerView.showProgressBar()
        registerInteractor.signUp(email, password, repeatPassword, check)
    }

    override fun goLogin() {
        registerView.goLogin()
    }

    override fun signUpSuccess() {
        registerView.hideProgressBar()
        registerView.goHome()
    }

    override fun signUpError(error: String) {
        registerView.hideProgressBar()
        registerView.signUpError(error)
    }

    override fun hasFocus(hasFocus: Boolean, id: Int) {
        if (hasFocus) {
            registerView.hasFocus(true, id)
        } else {
            registerView.noHasFocus(false, id)
        }
    }
}
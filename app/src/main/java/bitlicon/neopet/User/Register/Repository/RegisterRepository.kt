package bitlicon.neopet.User.Register.Repository

interface RegisterRepository {

    fun sigUp(email: String, password: String, repeatPassword: String, check: Boolean)
}
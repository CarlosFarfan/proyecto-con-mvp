package bitlicon.neopet.User.PaymentDetail.View

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.Button

import bitlicon.neopet.R
import bitlicon.neopet.Ecommerce.ShoppingCart.View.SCartActivity

class PaymentDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_detail)

        val toolbar = findViewById<Toolbar>(R.id.t_payment)
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.text_title_payment)

        val totalPay = intent.getStringExtra("TotalPay")
        val btnPay = findViewById<Button>(R.id.btn_pay_payment)
        btnPay.text = "pagar $totalPay"
    }

    override fun onBackPressed() {
        startActivity(Intent(this@PaymentDetailActivity, SCartActivity::class.java))
        finish()
    }
}
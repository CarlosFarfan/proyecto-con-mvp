package bitlicon.neopet.User.Login.Presenter

import bitlicon.neopet.User.Login.Interactor.LoginInteractor
import bitlicon.neopet.User.Login.Interactor.LoginInteractorImpl
import bitlicon.neopet.User.Login.View.LoginView

class LoginPresenterImpl(private val loginView: LoginView) : LoginPresenter {

    private val loginInteractor: LoginInteractor

    init {
        loginInteractor = LoginInteractorImpl(this)
    }

    override fun logIn(email: String, password: String) {
        loginView.showProgressBar()
        loginInteractor.logIn(email, password)
    }

    override fun loginSuccess() {
        loginView.hideProgressBar()
        loginView.goHome()
    }

    override fun loginError(error: String) {
        loginView.hideProgressBar()
        loginView.loginError(error)
    }

    override fun goHome() {
        loginView.goHome()
    }

    override fun goCreateAccount() {
        loginView.goCreateAccount()
    }

    override fun hasFocus(hasFocus: Boolean, id: Int) {
        if (hasFocus) {
            loginView.hasFocus(true, id)
        } else {
            loginView.noHasFocus(false, id)
        }
    }
}
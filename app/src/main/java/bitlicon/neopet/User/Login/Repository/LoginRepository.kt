package bitlicon.neopet.User.Login.Repository

interface LoginRepository {

    fun logIn(email: String, password: String)
}
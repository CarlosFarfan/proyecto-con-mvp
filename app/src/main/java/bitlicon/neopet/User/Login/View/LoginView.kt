package bitlicon.neopet.User.Login.View

interface LoginView {

    fun showProgressBar()
    fun hideProgressBar()
    fun goHome()
    fun goCreateAccount()
    fun loginError(error: String)
    fun hasFocus(hasFocus: Boolean, id: Int)
    fun noHasFocus(hasFocus: Boolean, id: Int)
}
package bitlicon.neopet.User.Login.Interactor

import bitlicon.neopet.User.Login.Presenter.LoginPresenter
import bitlicon.neopet.User.Login.Repository.LoginRepository
import bitlicon.neopet.User.Login.Repository.LoginRepositoryImpl

class LoginInteractorImpl(loginPresenter: LoginPresenter) : LoginInteractor {

    private val loginRepository: LoginRepository

    init {
        loginRepository = LoginRepositoryImpl(loginPresenter)
    }

    override fun logIn(email: String, password: String) {
        loginRepository.logIn(email, password)
    }
}
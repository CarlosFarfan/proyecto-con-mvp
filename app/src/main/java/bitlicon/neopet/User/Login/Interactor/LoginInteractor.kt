package bitlicon.neopet.User.Login.Interactor

interface LoginInteractor {

    fun logIn(email: String, password: String)
}
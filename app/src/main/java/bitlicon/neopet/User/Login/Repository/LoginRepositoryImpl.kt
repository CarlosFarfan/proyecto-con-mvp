package bitlicon.neopet.User.Login.Repository

import android.util.Patterns

import bitlicon.neopet.User.Login.Presenter.LoginPresenter

class LoginRepositoryImpl(private val loginPresenter: LoginPresenter) : LoginRepository {

    override fun logIn(email: String, password: String) {

        if (email.isEmpty()) {
            loginPresenter.loginError("Error con email")
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginPresenter.loginError("Email incorrecto")
        } else if (password.isEmpty()) {
            loginPresenter.loginError("Error con contraseña")
        } else {
            loginPresenter.loginSuccess()
        }
    }
}
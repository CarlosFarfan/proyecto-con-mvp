package bitlicon.neopet.User.Login.Presenter

interface LoginPresenter {

    fun logIn(email: String, password: String)
    fun loginSuccess()
    fun loginError(error: String)
    fun goHome()
    fun goCreateAccount()
    fun hasFocus(hasFocus: Boolean, id: Int)
}
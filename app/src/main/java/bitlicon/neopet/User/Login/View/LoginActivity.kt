package bitlicon.neopet.User.Login.View

import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView

import bitlicon.neopet.User.Login.Presenter.LoginPresenter
import bitlicon.neopet.User.Login.Presenter.LoginPresenterImpl
import bitlicon.neopet.MainActivity
import bitlicon.neopet.R
import bitlicon.neopet.User.Register.View.RegisterActivity
import bitlicon.neopet.Utils.Functions
import bitlicon.neopet.Utils.Preferences

class LoginActivity : AppCompatActivity(), LoginView {

    private var flPassword: FrameLayout? = null
    private var etPassword: EditText? = null
    private var flEmail: FrameLayout? = null
    private var etEmail: EditText? = null

    private var loginPresenter: LoginPresenter? = null
    private var preferences: Preferences? = null
    private var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        preferences = Preferences(this@LoginActivity)
        loginPresenter = LoginPresenterImpl(this)

        flEmail = findViewById(R.id.fl_email)
        flPassword = findViewById(R.id.fl_password)
        etEmail = findViewById(R.id.et_email)
        etPassword = findViewById(R.id.et_password)
        val tvForgotPassword = findViewById<TextView>(R.id.tv_forgot_password)
        val btnLogIn = findViewById<Button>(R.id.btn_log_in)
        val tvFollow = findViewById<TextView>(R.id.tv_follow)
        val tvCreateAccount = findViewById<TextView>(R.id.tv_create_account)

        etEmail!!.setOnFocusChangeListener { _, hasFocus -> loginPresenter?.hasFocus(hasFocus, R.id.et_email) }
        etPassword!!.setOnFocusChangeListener { _, hasFocus -> loginPresenter?.hasFocus(hasFocus, R.id.et_password) }

        btnLogIn.setOnClickListener { loginPresenter?.logIn(etEmail?.text.toString().trim(), etPassword!!.text.toString().trim()) }
        tvFollow.setOnClickListener { loginPresenter?.goHome() }
        tvCreateAccount.setOnClickListener { loginPresenter?.goCreateAccount() }
    }

    override fun showProgressBar() {
        dialog = Functions.showProgressDialog("Autenticando", "Espere un momento", this@LoginActivity)
    }

    override fun hideProgressBar() {
        dialog?.dismiss()
    }

    override fun hasFocus(hasFocus: Boolean, id: Int) {
        when (id) {
            R.id.et_email -> flEmail?.background = ContextCompat.getDrawable(this@LoginActivity, R.drawable.background_high_card)
            R.id.et_password -> flPassword?.background = ContextCompat.getDrawable(this@LoginActivity, R.drawable.background_high_card)
        }
    }

    override fun noHasFocus(hasFocus: Boolean, id: Int) {
        when (id) {
            R.id.et_email -> flEmail?.background = ContextCompat.getDrawable(this@LoginActivity, R.drawable.background_low_card)
            R.id.et_password -> flPassword?.background = ContextCompat.getDrawable(this@LoginActivity, R.drawable.background_low_card)
        }
    }

    override fun goHome() {
        preferences?.showMyPurchases(false)
        preferences?.showProducts(true)
        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
        finish()
    }

    override fun goCreateAccount() {
        startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
        finish()
    }

    override fun loginError(error: String) {
        Functions.showAlertDialog(error, this@LoginActivity)
    }
}
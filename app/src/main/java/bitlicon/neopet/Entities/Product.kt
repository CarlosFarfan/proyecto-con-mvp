package bitlicon.neopet.Entities

class Product(val productID: Int, val product: String, val typeAnimal: String, val availability: Int, var priceByQuantity: Double?, var price: Double?, val image: String, val shortInformation: String, val weight: String) {
    var quantity: Int = 0
    var isAdd = false
}
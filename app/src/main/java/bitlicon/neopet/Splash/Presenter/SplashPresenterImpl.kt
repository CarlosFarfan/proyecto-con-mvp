package bitlicon.neopet.Splash.Presenter

import bitlicon.neopet.Splash.View.SplashView

class SplashPresenterImpl(private val splashView: SplashView) : SplashPresenter {

    override fun goLogin() {
        splashView.goLogin()
    }

    override fun goHome() {
        splashView.goHome()

    }
}
package bitlicon.neopet.Splash.Presenter

interface SplashPresenter {

    fun goLogin()
    fun goHome()
}
package bitlicon.neopet.Splash.View

interface SplashView {

    fun goLogin()
    fun goHome()
}
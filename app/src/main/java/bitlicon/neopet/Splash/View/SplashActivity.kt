package bitlicon.neopet.Splash.View

import android.content.Intent
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import bitlicon.neopet.User.Login.View.LoginActivity
import bitlicon.neopet.MainActivity
import bitlicon.neopet.R
import bitlicon.neopet.Splash.Presenter.SplashPresenter
import bitlicon.neopet.Splash.Presenter.SplashPresenterImpl

class SplashActivity : AppCompatActivity(), SplashView {

    private var splashPresenter: SplashPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        splashPresenter = SplashPresenterImpl(this)

        Handler().postDelayed({ splashPresenter?.goLogin() }, 3000)
    }

    override fun goLogin() {
        startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
        finish()
    }

    override fun goHome() {
        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
        finish()
    }
}
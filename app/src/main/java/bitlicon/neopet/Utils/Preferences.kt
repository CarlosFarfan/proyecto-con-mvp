package bitlicon.neopet.Utils

import android.content.Context
import android.content.SharedPreferences

class Preferences(private val context: Context) {

    companion object {

        private val PREF_NAME = "AndroidNeoPet"
        private val SHOW_MY_PURCHASES = "ShowMyPurchases"
        private val SHOW_PRODUCTS = "ShowProducts"
    }

    private val editor: SharedPreferences.Editor
    private val pref: SharedPreferences
    private val PRIVATE_MODE = 0

    init {
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun showMyPurchases(show: Boolean) {
        editor.putBoolean(SHOW_MY_PURCHASES, show)
        editor.commit()
    }

    fun showProducts(show: Boolean) {
        editor.putBoolean(SHOW_PRODUCTS, show)
        editor.commit()
    }

    val isShowMyPurchases: Boolean
        get() = pref.getBoolean(SHOW_MY_PURCHASES, false)

    val isShowProducts: Boolean
        get() = pref.getBoolean(SHOW_PRODUCTS, false)
}
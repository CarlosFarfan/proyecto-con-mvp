package bitlicon.neopet.Utils.Adapter

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import bitlicon.neopet.Entities.Category
import bitlicon.neopet.R

class CategoryAdapter(private val activity: Activity, private var recyclerView: RecyclerView, private val categories: List<Category>, private var previousPosition: Int = -1) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CategoryAdapter.CategoryViewHolder {
        return CategoryViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_category, parent, false))
    }

    override fun onBindViewHolder(holder: CategoryViewHolder?, position: Int) {
        val category: Category = categories[position]

        holder?.imageCategory?.setImageDrawable(ContextCompat.getDrawable(activity, category.DrawableID))

        if (previousPosition == position) {
            holder?.background?.background = ContextCompat.getDrawable(activity, R.drawable.background_select)
            holder?.imageCategory?.imageTintList = ContextCompat.getColorStateList(activity, android.R.color.white)
        } else {
            holder?.background?.background = ContextCompat.getDrawable(activity, R.drawable.background_unselect)
            holder?.imageCategory?.imageTintList = ContextCompat.getColorStateList(activity, R.color.colorPrimary)
        }
    }

    inner class CategoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal val imageCategory: ImageView = view.findViewById(R.id.option)
        internal val background: View = view.findViewById(R.id.background)

        init {
            background.setOnClickListener {
                var _holder: CategoryViewHolder?
                val _view: CategoryViewHolder?

                if (previousPosition == -1) {
                    previousPosition = adapterPosition
                    _view = recyclerView.findViewHolderForAdapterPosition(previousPosition) as CategoryViewHolder?

                    _view?.background?.background = ContextCompat.getDrawable(activity, R.drawable.background_select)
                    _view?.imageCategory?.imageTintList = ContextCompat.getColorStateList(activity, android.R.color.white)
                } else if (adapterPosition == previousPosition) {
                    _view = recyclerView.findViewHolderForAdapterPosition(previousPosition) as CategoryViewHolder?

                    _view?.background?.background = ContextCompat.getDrawable(activity, R.drawable.background_unselect)
                    _view?.imageCategory?.imageTintList = ContextCompat.getColorStateList(activity, R.color.colorPrimary)

                    previousPosition = -1
                } else {
                    _holder = recyclerView.findViewHolderForAdapterPosition(previousPosition) as CategoryViewHolder?

                    if (_holder != null) {
                        _holder.background.background = ContextCompat.getDrawable(activity, R.drawable.background_unselect)
                        _holder.imageCategory.imageTintList = ContextCompat.getColorStateList(activity, R.color.colorPrimary)
                    } else {
                        notifyItemChanged(previousPosition)
                    }

                    _holder = recyclerView.findViewHolderForAdapterPosition(adapterPosition) as CategoryViewHolder?

                    if (_holder != null) {
                        _holder.background.background = ContextCompat.getDrawable(activity, R.drawable.background_select)
                        _holder.imageCategory.imageTintList = ContextCompat.getColorStateList(activity, android.R.color.white)
                    }
                    previousPosition = adapterPosition
                }

                if (onItemClickListener != null && previousPosition != -1) {
                    onItemClickListener?.onClick(categories[adapterPosition].CategoryID)
                } else if (onItemClickListener != null) {
                    onItemClickListener?.onClick(-1)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onClick(option: Int)
    }
}
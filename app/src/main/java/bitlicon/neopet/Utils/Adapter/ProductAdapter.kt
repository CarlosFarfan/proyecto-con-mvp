package bitlicon.neopet.Utils.Adapter

import android.app.Activity
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

import com.bumptech.glide.Glide
import com.mikepenz.actionitembadge.library.ActionItemBadge

import bitlicon.neopet.Entities.Product
import bitlicon.neopet.Ecommerce.ProductDetail.View.ProductDetailActivity
import bitlicon.neopet.R
import bitlicon.neopet.Utils.Functions

class ProductAdapter(private val products: MutableList<Product>, private val activity: Activity, private val item: MenuItem, private val enterID: Int) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var badgeCount = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductAdapter.ProductViewHolder {
        return ProductViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_shop, parent, false))
    }

    override fun onBindViewHolder(holder: ProductAdapter.ProductViewHolder, position: Int) {
        val product = products[position]

        Glide.with(activity).load(product.image).into(holder.imageProduct)
        holder.product.text = product.product
        holder.typeAnimal.text = product.typeAnimal
        holder.priceProduct.text = "S/." + Functions.formatPrice(product.price)
        holder.shortInformation.text = product.shortInformation

        if (product.isAdd) {
            holder.additionProduct.visibility = View.VISIBLE
            holder.btnAddProduct.visibility = View.GONE
        } else {
            holder.additionProduct.visibility = View.GONE
            holder.btnAddProduct.visibility = View.VISIBLE
        }

        holder.btnAddProduct.setOnClickListener {
            product.isAdd = true
            holder.additionProduct.visibility = View.VISIBLE
            holder.btnAddProduct.visibility = View.GONE

            badgeCount += 1
            ActionItemBadge.update(activity, item, ContextCompat.getDrawable(activity, R.drawable.ic_shop), ActionItemBadge.BadgeStyles.DARK_GREY, badgeCount)
        }
    }

    inner class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal val product: TextView = view.findViewById(R.id.product_card)
        internal val typeAnimal: TextView = view.findViewById(R.id.type_animal_card)
        internal val priceProduct: TextView = view.findViewById(R.id.price_product_card)
        internal val shortInformation: TextView = view.findViewById(R.id.short_information_card)
        internal val additionProduct: TextView = view.findViewById(R.id.addition_product_card)
        internal val imageProduct: ImageView = view.findViewById(R.id.image_product_card)
        internal val btnAddProduct: Button = view.findViewById(R.id.btn_add_product_card)

        init {
            view.setOnClickListener {
                val intent = Intent(activity, ProductDetailActivity::class.java)
                intent.putExtra("EnterID", enterID)
                activity.startActivity(intent)
                activity.finish()
            }
        }
    }

    override fun getItemCount(): Int {
        return products.size
    }

    fun addAll() {
        for (i in products.indices) {
            products[i].isAdd = true
        }
        ActionItemBadge.update(activity, item, ContextCompat.getDrawable(activity, R.drawable.ic_shop), ActionItemBadge.BadgeStyles.DARK_GREY, products.size)
        notifyDataSetChanged()
    }
}
package bitlicon.neopet.Utils

import android.app.Activity
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.TextView

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

import bitlicon.neopet.R

object Functions {

    fun formatPrice(price: Double?): String {
        val formatSymbols = DecimalFormatSymbols(Locale.getDefault())
        formatSymbols.decimalSeparator = '.'
        val decimalFormat = DecimalFormat("###,###,##0.00", formatSymbols)
        return decimalFormat.format(price)
    }

    fun showAlertDialog(message: String, activity: Activity) {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialogInterface, _ -> dialogInterface.dismiss() }

        val dialog = builder.create()
        dialog.setCancelable(false)
        dialog.show()
    }

    fun showProgressDialog(title: String, message: String, activity: Activity): AlertDialog {
        val builder = AlertDialog.Builder(activity)
        val view = LayoutInflater.from(activity).inflate(R.layout.progress_dialog, null)

        val Title = view.findViewById<TextView>(R.id.title_progress)
        val Message = view.findViewById<TextView>(R.id.message_progress)

        Title.text = title
        Message.text = message

        builder.setView(view)
        val dialog = builder.create()
        dialog.setCancelable(false)
        dialog.show()

        return dialog
    }
}